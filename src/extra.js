// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
// This File is From rExtension System
// Licensed under BSD-2-Caluse
// File: main.js (rExtension/libstd/src/main.js)
// Content:	StandardLib Extra Code (Add new functions in the origin methods and Fix some methods)
// Copyright (c) 2022 rExtension System All rights reserved
// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

"use strict"

{
	if (module.parent) {
		// To Do
	}
	else {
		let changes = {};

		// Game
		{
			const Filter = function (origin, fun, result) {
				if (!Array.isArray(result)) result = new Array;
				if (typeof fun != "function") fun = lib.filter.all;
				return result.concat(origin.filter(current => !current.isOut() && fun(current)))
			};

			const Count = function (origin, fun) {
				if (typeof fun != "function") fun = lib.filter.all;
				return origin.reduce((result, current) => {
					if (current.isOut()) return result
					const fun_result = fun(current);
					if (!fun_result) return result
					return result + (typeof fun_result == "number" ? fun_result : 1)
				}, 0)
			};

			changes.game = {
				countPlayer(fun = lib.filter.all) {
					return Count(game.players, fun)
				},

				countPlayer2(fun = lib.filter.all) {
					return Count(game.players.concat(game.dead), fun)
				},

				filterPlayer(fun = lib.filter.all, result = new Array) {
					return Filter(game.players, fun, result)
				},

				filterPlayer2(fun = lib.filter.all, result = new Array) {
					return Filter(game.players.concat(game.dead), fun, result)
				},

				hasPlayer(fun = lib.filter.all) {
					if (typeof fun != "function") fun = lib.filter.all;
					return game.players.some(current => !current.isOut() && fun(current))
				},

				hasPlayer2(fun = lib.filter.all) {
					if (typeof fun != "function") fun = lib.filter.all;
					return game.players.concat(game.dead).some(current => !current.isOut() && fun(current))
				}
			};
		}

		// Content
		{
			changes.content = {
				die() {
					"step 0"
					event.forceDie = true;
					if (_status.roundStart == player) {
						_status.roundStart = player.next || player.getNext() || game.players[0];
					}
					if (ui.land && ui.land.player == player) {
						game.addVideo("destroyLand");
						ui.land.destroy();
					}
					var unseen = false;
					if (player.classList.contains("unseen")) {
						player.classList.remove("unseen");
						unseen = true;
					}
					var logvid = game.logv(player, "die", source);
					event.logvid = logvid;
					if (unseen) {
						player.classList.add("unseen");
					}
					if (source) {
						game.log(player, "被", source, "杀害");
						if (source.stat[source.stat.length - 1].kill == undefined) {
							source.stat[source.stat.length - 1].kill = 1;
						}
						else {
							source.stat[source.stat.length - 1].kill++;
						}
					}
					else {
						game.log(player, "阵亡")
					}

					game.broadcastAll(function (player) {
						player.classList.add("dead");
						player.removeLink();
						player.classList.remove("turnedover");
						player.classList.remove("out");
						player.node.count.innerHTML = "0";
						player.node.hp.hide();
						player.node.equips.hide();
						player.node.count.hide();
						player.previous.next = player.next;
						player.next.previous = player.previous;
						game.players.remove(player);
						game.dead.push(player);
						_status.dying.remove(player);

						if (lib.config.background_speak) {
							if (lib.character[player.name] && lib.character[player.name][4].contains("die_audio")) {
								game.playAudio("die", player.name);
							}
							else if (lib.character[player.name] && lib.character[player.name][4].some(item => item.match(/^die:/))) {
								let index = lib.character[player.name][4].findIndex(item => {
									if (typeof item == "string") {
										return item.match(/^die:/) != null;
									}
									return false;
								});
								let audio = lib.character[player.name][4][index]
									.replace(/^die:/, "")
									.replace(/ext:/, "extension/");
								game.playAudio("..", audio);
							}
							else if (lib.character[player.name] && lib.character[player.name][4].some(item => item.match(/^died:/))) {
								let index = lib.character[player.name][4].findIndex(item => {
									if (typeof item == "string") {
										return item.match(/^died:/) != null;
									}
									return false;
								});
								let audio = lib.character[player.name][4][index]
									.replace(/^died:/, "")
									.replace(/ext:/, "extension/");
								game.playAudio("..", audio, player.name);
							}
							else {
								game.playAudio("die", player.name, function () {
									game.playAudio("die", player.name.slice(player.name.indexOf("_") + 1));
								});
							}
						}
					}, player);

					game.addVideo("diex", player);
					if (event.animate !== false) {
						player.$die(source);
					}
					if (player.hp != 0) {
						player.changeHp(0 - player.hp, false).forceDie = true;
					}
					"step 1"
					if (player.dieAfter) player.dieAfter(source);
					"step 2"
					event.trigger("die");
					"step 3"
					if (player.isDead()) {
						if (!game.reserveDead) {
							for (var mark in player.marks) {
								player.unmarkSkill(mark);
							}
							while (player.node.marks.childNodes.length > 1) {
								player.node.marks.lastChild.remove();
							}
							game.broadcast(function (player) {
								while (player.node.marks.childNodes.length > 1) {
									player.node.marks.lastChild.remove();
								}
							}, player);
						}
						for (var i in player.tempSkills) {
							player.removeSkill(i);
						}
						var skills = player.getSkills();
						for (var i = 0; i < skills.length; i++) {
							if (lib.skill[skills[i]].temp) {
								player.removeSkill(skills[i]);
							}
						}
						if (_status.characterlist) {
							if (lib.character[player.name]) _status.characterlist.add(player.name);
							if (lib.character[player.name1]) _status.characterlist.add(player.name1);
							if (lib.character[player.name2]) _status.characterlist.add(player.name2);
						}
						event.cards = player.getCards("hejs");
						if (event.cards.length) {
							player.discard(event.cards).forceDie = true;
							//player.$throw(event.cards,1000);
						}
					}
					"step 4"
					if (player.dieAfter2) player.dieAfter2(source);
					"step 5"
					game.broadcastAll(function (player) {
						if (game.online && player == game.me && !_status.over && !game.controlOver && !ui.exit) {
							if (lib.mode[lib.configOL.mode].config.dierestart) {
								ui.create.exit();
							}
						}
					}, player);
					if (!_status.connectMode && player == game.me && !_status.over && !game.controlOver) {
						ui.control.show();
						if (get.config("revive") && lib.mode[lib.config.mode].config.revive && !ui.revive) {
							ui.revive = ui.create.control("revive", ui.click.dierevive);
						}
						if (get.config("continue_game") && !ui.continue_game && lib.mode[lib.config.mode].config.continue_game && !_status.brawl && !game.no_continue_game) {
							ui.continue_game = ui.create.control("再战", game.reloadCurrent);
						}
						if (get.config("dierestart") && lib.mode[lib.config.mode].config.dierestart && !ui.restart) {
							ui.restart = ui.create.control("restart", game.reload);
						}
					}

					if (!_status.connectMode && player == game.me && !game.modeSwapPlayer) {
						// _status.auto=false;
						if (ui.auto) {
							// ui.auto.classList.remove("glow");
							ui.auto.hide();
						}
						if (ui.wuxie) ui.wuxie.hide();
					}

					if (typeof _status.coin == "number" && source && !_status.auto) {
						if (source == game.me || source.isUnderControl()) {
							_status.coin += 10;
						}
					}
					if (source && lib.config.border_style == "auto" && (lib.config.autoborder_count == "kill" || lib.config.autoborder_count == "mix")) {
						switch (source.node.framebg.dataset.auto) {
							case "gold": case "silver": source.node.framebg.dataset.auto = "gold"; break;
							case "bronze": source.node.framebg.dataset.auto = "silver"; break;
							default: source.node.framebg.dataset.auto = lib.config.autoborder_start || "bronze";
						}
						if (lib.config.autoborder_count == "kill") {
							source.node.framebg.dataset.decoration = source.node.framebg.dataset.auto;
						}
						else {
							var dnum = 0;
							for (var j = 0; j < source.stat.length; j++) {
								if (source.stat[j].damage != undefined) dnum += source.stat[j].damage;
							}
							source.node.framebg.dataset.decoration = "";
							switch (source.node.framebg.dataset.auto) {
								case "bronze": if (dnum >= 4) source.node.framebg.dataset.decoration = "bronze"; break;
								case "silver": if (dnum >= 8) source.node.framebg.dataset.decoration = "silver"; break;
								case "gold": if (dnum >= 12) source.node.framebg.dataset.decoration = "gold"; break;
							}
						}
						source.classList.add("topcount");
					}
				}
			};
		}

		for (let place in changes) {
			const item = ["game"].includes(place) ? global[place] : lib.element[place];
			if (!item._super) item._super = {};
			for (let key in changes[place]) {
				item._super[key] = item[key];
				item[key] = changes[place][key];
			}
		}
	}
}
