// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
// This File is From rExtension System
// Licensed under BSD-2-Caluse
// File: main.js (rExtension/libstd/src/main.js)
// Content:	StandardLib Main Module
// Copyright (c) 2022 rExtension System All rights reserved
// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

"use strict"

{
	if (module.parent) {
		
		class StandardLib {
			path(...dirs) {
				return `${lib.assetURL}./${dirs.join("/")}`
			}
		}

		module.exports = StandardLib;
	}
}
