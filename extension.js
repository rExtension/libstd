// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
// This File is From rExtension System
// Licensed under BSD-2-Caluse
// File: extension.js (rExtension/libstd/extension.js)
// Content:	StandardLib Extension File
// Copyright (c) 2022 rExtension System All rights reserved
// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

"use strict"

game.import("extension", (lib, game, ui, get, ai, _status) => {
	if (typeof require != "function") {
		game.removeExtension("StandardLib", false);
		throw "没有Node.js环境，无法导入此扩展";
	}

	return {
		name: "StandardLib",
		intro: "rExtension 标准代码库",
		editable: false,
		config: {},
		package: {
			author: "rExtension",
			version: "0.1.0",
		},
		content(_config, _pack) {
			lib.init.js(lib.assetURL + "./extension/StandardLib", "module", () => {
				game.players.forEach(current => {
					for (let key in lib.element.player) {
						const value = lib.element.player[key];
						current[key] = value;
					}
				})
			})
		},
		precontent(config) {
			if (!global.lib) global.lib = lib;
			if (!global.game) global.game = game;
			if (!global.ui) global.ui = ui;
			if (!global.get) global.get = get;
			if (!global.ai) global.ai = ai;
		},
	}
})
