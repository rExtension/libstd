// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
// This File is From rExtension System
// Licensed under BSD-2-Caluse
// File: module.js (rExtension/libstd/module.js)
// Content:	StandardLib Module File
// Copyright (c) 2022 rExtension System All rights reserved
// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

"use strict"

var std;

{
	// Load Standard Codes
	{
		const Files = ["main", "extra"];
		lib.init.js(lib.assetURL + "./extension/StandardLib/src", Files)
	}

	// Import Standard Module
	{
		const StandardLib = require(lib.assetURL + "./extension/StandardLib/src/main.js");

		if (StandardLib) std = new StandardLib
	}

}
